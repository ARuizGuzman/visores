function initMap(){
    var mymap = L.map('visor').setView([4.628095, -74.065458], 13);
    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoiYW5kcmVzcnVpeiIsImEiOiJja2N3aGltbnMwZHVsMnJsbHg2ejVtNXR2In0.EPBFj3ty-HJSxO2_ZX3O5Q', {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        maxZoom: 18,
        id: 'mapbox/streets-v11',
        tileSize: 512,
        zoomOffset: -1,
    }).addTo(mymap);

    var marker = L.marker([4.628095, -74.065458]).addTo(mymap);
    marker.bindPopup("<b>Universidad Distrital FJC</b><br>Facultad Ingenieria.").openPopup();

    var marker = L.marker([4.658527, -74.093463]).addTo(mymap);
    marker.bindPopup("<b>Parque</b><br>Simon Bolivar.").openPopup();

    var marker = L.marker([4.663562, -74.087094]).addTo(mymap);
    marker.bindPopup("<b>Parque</b><br>El Salitre").openPopup();

    var marker = L.marker([4.597584, -74.087094]).addTo(mymap);
    marker.bindPopup("<b>Parque</b><br>Tercer Milenio.").openPopup();

    var marker = L.marker([4.586130, -74.110878]).addTo(mymap);
    marker.bindPopup("<b>La Mejor Panaderia</b><br>P. Centenario.").openPopup();

    var circle = L.circle([4.628095, -74.065458], {
        color: 'red',
        fillColor: '#f03',
        fillOpacity: 0.3,
        radius: 100
    }).addTo(mymap);
    circle.bindPopup("Ubicacion de la mejor Universidad");

    var polygon = L.polygon([
        [4.656910, -74.100412],
        [4.671245, -74.089153],
        [4.660976, -74.082322],
        [4.658672, -74.078180],
        [4.652578, -74.080891],
        [4.655323, -74.085760],
        [4.652361, -74.093422]
    ]).addTo(mymap); 
    polygon.bindPopup("Zona de Parques");

}


